//
//  Constants.swift
//  RSSReader
//
//  Created by 伊藤総一郎 on 4/20/15.
//  Copyright (c) 2015 susieyy. All rights reserved.
//

import Foundation

struct Constants {
    struct article_url {
        static let GOOGLE_NEWS = "https://news.google.com/news?hl=ja&ned=us&ie=UTF-8&oe=UTF-8&output=rss&q=%E4%B8%89%E4%BB%A3%E7%9B%AE&num=3"
        static let ORICON_NEWS = "https://www.kimonolabs.com/api/a5jver36?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
        static let NAVER_URL = "https://www.kimonolabs.com/api/95z4lq8w?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
        static let NATARY_URL = "https://www.kimonolabs.com/api/acrprn9c?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
        static let CRANCK_IN_URL = "https://www.kimonolabs.com/api/dvlwes42?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    
    struct nend_id {
        static let API_ID = "fb87edcd496eb639c6bb2d2bab76d1e1f166c30c"
        static let SPOT_ID = "346573"
    }
    
    struct news_json_key {
        static let pubDate = "pubDate"
    }
    
    struct server_api {
//        static let CREATE_COMMNET = "https://edmappback.herokuapp.com/comment/create"
//        static let GET_COMMENT = "https://edmappback.herokuapp.com/comment/find"
//        static let GET_PLAY = "https://edmappback.herokuapp.com/play/getNum"
//        static let CREATE_PLAY = "https://edmappback.herokuapp.com/play/create"
        
        static let CREATE_COMMNET = "http://localhost:9000/comment/create"
        static let GET_COMMENT =    "http://localhost:9000/comment/find"
        static let GETNUM_COMMENT = "http://localhost:9000/comment/getNum"
        static let GET_PLAY = "http://localhost:9000/play/getNum"
        static let CREATE_PLAY = "http://localhost:9000/play/create"
        static let GET_FAVO = "http://localhost:9000/favorite/getNum"
        static let CREATE_FAVO = "http://localhost:9000/favorite/create"
    }
    
    struct article_data {
        static let TITLE = "title"
        static let LINK = "link"
        static let AUTHOR = "author"
        static let CREATED_AT = "created_at"
        static let DATE = "date"
        static let HREF = "href"
        static let TEXT = "text"
        static let PLACE = "place"
        static let VALUE = "value"
        static let WHEN = "when"
        static let IMAGE_URL = "image_url"
        static let VIDEO_ID = "videoId"
        static let PUBLISH_AT = "publishAt"
        static let TICKET = "ticket"
    }
    
    struct blog {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/ckcy77lw?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    
    struct shop_apparel {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/846g1ifc?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    
    struct shop_accessory {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/ck7xfxnw?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"        
    }
    struct shop_hats {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/3u5sxpds?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    
    struct shop_poster {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/2c977h6a?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    
    struct shop_music {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/73wj2qhe?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }

    struct tour {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/aotlttye?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    struct news_photo {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/3vn30goa?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    struct lyrics {
        static let OFFICIAL_URL = "https://www.kimonolabs.com/api/7kkf3k4q?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }

    
    struct ticket {
        static let TICKET_URL = "https://www.kimonolabs.com/api/6atq0zuo?apikey=O1LWGKfEhBnwnOmTTuxzTO5UiTYhLuLu"
    }
    
    struct youtube {
        static let API_KEY = "AIzaSyCcfEgQ_6qLIV5STXnZnLo040NzEmZuVZ4"
        static let WORD = "J Soul Brothers"
    }
    
    struct message {
        static let LOADING = "Loading..."
        static let UPDATING = "updating..."
    }
    
    struct title {
        static let BLOG = "ブログ"
        static let NEWS = "ニュース"
        static let MOVIE = "動画"
        static let OTHER = "その他"
        static let TICKET = "チケット"
    }
    
    struct frame {
        static let X = 10;
        static let Y = 420
        static let WIDTH = 660
        static let HEIGHT = 10
    }
}