//
//  Favorite.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 6/10/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//

import Foundation
//import Realm

class Favorite : RLMObject {
    dynamic var id = 0
    dynamic var video_id = ""
    dynamic var name = ""
    dynamic var thumbnail = ""
    
    dynamic var folder_id = ""
}


