//
//  FavoritesViewController.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 6/10/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var favoriteTable: UITableView!
    var items: RLMResults {
        get {
            return Favorite.allObjects()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        favoriteTable.delegate = self;
        favoriteTable.dataSource = self;
        favoriteTable.opaque = true;
        favoriteTable.backgroundColor = UIColor.clearColor()
        favoriteTable.backgroundView = nil;
        favoriteTable.separatorStyle = UITableViewCellSeparatorStyle.None
        favoriteTable.bounces = false;
        favoriteTable.scrollsToTop = false;

        let nibName = UINib(nibName: "FavoriteTableViewCell", bundle:nil)
        favoriteTable.registerNib(nibName, forCellReuseIdentifier: "Cell")

           
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 229
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count.hashValue
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.favoriteTable.dequeueReusableCellWithIdentifier("Cell") as! FavoriteTableViewCell
        let item = items.objectAtIndex(UInt(indexPath.row)) as! Favorite
        
        cell.titleLabel.text = item.name
        
//        let item = self.items[indexPath.row] as! NSDictionary
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        let item = self.items[indexPath.row] as! NSDictionary
//        self.performSegueWithIdentifier("VideoViewController", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    }

}
