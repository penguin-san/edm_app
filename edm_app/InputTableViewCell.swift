//
//  InputTableViewCell.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 6/1/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//

import UIKit

class InputTableViewCell: UITableViewCell {

    @IBOutlet weak var inputLabel: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    var video_id: String!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func postComment(sender: AnyObject) {
        var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var param : Dictionary<String, String> = ["video_id" : video_id, "user_uuid": (appDelegate.UUID as String), "content": (inputLabel.text as String)]
        Request.get(Constants.server_api.CREATE_COMMNET, params: param) {
            (data, error) in
            println(data)
            println("success get request")
        }
        println(inputLabel.text)
        println("pushed button")
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
