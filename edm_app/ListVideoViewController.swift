//
//  ListVideoViewController.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 6/5/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//

import UIKit

class ListVideoViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    private var tableView: UITableView!

    var nextPageToken:NSString!
    var items = NSMutableArray()
    var videoTitles = [String]()
    var filteredVideoTitle = [String]()
    var nextVideoId: String!
    var channelId: String!
    final let API_KEY = Constants.youtube.API_KEY
    final let WORD:String = Constants.youtube.WORD
    var resultSearchController = UISearchController()
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        println(channelId)

        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: displayWidth, height: displayHeight - 30 ))
        
        tableView.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin|UIViewAutoresizing.FlexibleBottomMargin|UIViewAutoresizing.FlexibleWidth
        tableView.delegate = self;
        
        tableView.dataSource = self;
        tableView.opaque = true;
        tableView.backgroundColor = UIColor.clearColor()
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.bounces = false;
        tableView.scrollsToTop = false;
        self.view.addSubview(self.tableView)
        
        let nibName = UINib(nibName: "PVTableViewCell", bundle:nil)
        tableView.registerNib(nibName, forCellReuseIdentifier: "Cell")
        // Do any additional setup after loading the view.
        SVProgressHUD.showWithStatus(Constants.message.LOADING)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.request(false)
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
        })
        var param : Dictionary<String, String> = ["uuid": (appDelegate.UUID as String)]
        Request.createUser(param)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    func request(next: Bool) {
        let searchWord:String! = WORD.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        var urlString:String
        if(next) {
            println(self.nextPageToken)
            urlString = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=\(channelId)&key=\(API_KEY)"
        } else {
            urlString = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=\(channelId)&key=\(API_KEY)"
        }
        let url:NSURL! = NSURL(string:urlString)
        let urlRequest:NSURLRequest = NSURLRequest(URL:url)
        var data = NSURLConnection.sendSynchronousRequest(urlRequest, returningResponse: nil, error: nil)
        let dic:NSDictionary = NSJSONSerialization.JSONObjectWithData(
            data!,
            options: NSJSONReadingOptions.AllowFragments,
            error: nil) as! NSDictionary
        var json = JSON(dic)
        var prev_videoid:String = ""
        for (index: String, subJson: JSON) in json["items"] {
            var content : Dictionary<String, AnyObject> = ["" : ""]
            println(subJson)
            var params : Dictionary<String, String> = ["video_id" : subJson["snippet"]["resourceId"]["videoId"].stringValue]

            Request.get(Constants.server_api.GET_PLAY, params: params) {
                (data, error) in
                content["lengthOfPlay"] = data["length"].stringValue
                
            }
//            Request.get(Constants.server_api.GET_FAVO, params: params) {
//                (data, error) in
//                content["lengthOfFavo"] = data["length"].stringValue
//            }
//            Request.get(Constants.server_api.GETNUM_COMMENT, params: params) {
//                (data, error) in
//                content["lengthOfComment"] = data["length"].stringValue
//            }

            
            if(subJson["snippet"]["resourceId"]["kind"].stringValue == "youtube#video") {
                content[Constants.article_data.VIDEO_ID] = subJson["snippet"]["resourceId"]["videoId"].stringValue
                content[Constants.article_data.TITLE] = subJson["snippet"]["title"].stringValue
                content[Constants.article_data.IMAGE_URL] = subJson["snippet"]["thumbnails"]["default"]["url"].stringValue
                content[Constants.article_data.PUBLISH_AT] = subJson["snippet"]["publishedAt"].stringValue
                
                var status = true
                var params : Dictionary<String, String> = ["video_id" : content[Constants.article_data.VIDEO_ID] as! String]
                Request.get(Constants.server_api.GET_PLAY, params: params) {
                    (data, error) in
                    content["lengthOfPlay"] = data["length"].stringValue
                }
                Request.get(Constants.server_api.GET_FAVO, params: params) {
                    (data, error) in
                    content["lengthOfFavo"] = data["length"].stringValue
                }
                Request.get(Constants.server_api.GETNUM_COMMENT, params: params) {
                    (data, error) in
                    content["lengthOfComment"] = data["length"].stringValue
                }
            



                 prev_videoid = content[Constants.article_data.VIDEO_ID] as! String
                
                println(content)
                
                self.items.addObject(content)
            }
//            if(!content.isEmpty) {
//                self.items.addObject(content)
//            }
//            content.removeAll(keepCapacity: true)
        }
//        if(items.count%20 != 0) {
//            nextPageToken = nil
//        }
        
        self.tableView.reloadData()
    }

    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)
            && self.nextPageToken != nil
            ) {
                SVProgressHUD.showWithStatus(Constants.message.LOADING)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.request(true)
                    SVProgressHUD.dismiss()
                })
                
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 229
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println(items.count)
        return self.items.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! PVTableViewCell
        let item = self.items[indexPath.row] as! NSDictionary
        

        cell.backgroundColor = UIColor(netHex: 0x3f3f3f)
        cell.titleLabel.textColor = UIColor.whiteColor()
        cell.titleLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        cell.video_id = item[Constants.article_data.VIDEO_ID] as! String
        cell.thumbnail_str = item[Constants.article_data.IMAGE_URL] as! String
        cell.video_name = item[Constants.article_data.TITLE] as! String
        
        let imgURL: NSURL? = NSURL(string: item[Constants.article_data.IMAGE_URL] as! String)
        cell.cover.setImageWithURL(imgURL)
        var length : String! = "1"
        println(item)
        cell.playButton.setTitle(item["lengthOfPlay"] as? String, forState: .Normal)
        cell.favoriteButton.setTitle(item["lengthOfFavo"] as? String , forState: .Normal)
        cell.commentButton.setTitle(item["lengthOfComment"] as? String , forState: .Normal)
        
        var title = (item[Constants.article_data.TITLE] as? String)!.componentsSeparatedByString("-")
        if(title.count == 1) {
            cell.titleLabel.text = item[Constants.article_data.TITLE] as? String
        } else {
            cell.titleLabel.text = title[1]
        }
        
        cell.sendSubviewToBack(cell.cover)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = self.items[indexPath.row] as! NSDictionary
        var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)

        nextVideoId = item[Constants.article_data.VIDEO_ID] as! String

        var param : Dictionary<String, String> = ["video_id" : nextVideoId, "uuid" : (appDelegate.UUID as String)]
        Request.get(Constants.server_api.CREATE_PLAY, params: param) {
            (data, error) in
            println(data)
            println("success")
        }
        self.performSegueWithIdentifier("VideoViewController", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
                var view : VideoViewController = segue.destinationViewController as! VideoViewController
                view.videoId = nextVideoId
    }

    
    
}
