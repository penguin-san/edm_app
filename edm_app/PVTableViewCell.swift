//
//  PVTableViewCell.swift
//  RSSReader
//
//  Created by 伊藤総一郎 on 5/3/15.
//  Copyright (c) 2015 susieyy. All rights reserved.
//

import UIKit

class PVTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    var video_id : String!
    var thumbnail_str :String!
    var video_name :String!

    @IBAction func favoriteUpdate(sender: AnyObject) {
        var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        var param : Dictionary<String, String> = ["video_id" : video_id, "uuid": (appDelegate.UUID as String)]
        Request.get(Constants.server_api.CREATE_FAVO, params: param) {
            (data, error) in
            let realm = RLMRealm.defaultRealm()
            println("data = ")
            println(data["result"])
            if(data["result"] == "true") {
                var allObj:RLMResults {
                    get { return Favorite.allObjects()}}
                let favo = Favorite()
            
                favo.name = self.video_name
//                favo.id = allObj.count.hashValue + 1
                favo.video_id = self.video_id
                favo.thumbnail = self.thumbnail_str
                
                realm.beginWriteTransaction()
                realm.addObject(favo)
                realm.commitWriteTransaction()
                
                self.favoriteButton.setImage(UIImage(named: "Circular_auriculars_16 (1).png"), forState: .Normal)
            } else {
                realm.beginWriteTransaction()
                realm.deleteObjects(Favorite.objectsWithPredicate(NSPredicate(format: "video_id = %@", self.video_id)))
                realm.commitWriteTransaction()
                self.favoriteButton.setImage(UIImage(named: "Circular_auriculars_16.png"), forState: .Normal)

            }
            var all:RLMResults {
                get { return Favorite.allObjects()}}
            println(all)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
