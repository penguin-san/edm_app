//
//  PVViewController.swift
//  RSSReader
//
//  Created by 伊藤総一郎 on 5/3/15.
//  Copyright (c) 2015 susieyy. All rights reserved.
//

import UIKit

class PVViewController: UIViewController, UITableViewDelegate , UITableViewDataSource  {

    var titles = ["Problem", "Black", "One Last Time", "blog", "Twitter", "Live Information"]
    private var tableView: UITableView!
  
    var nextPageToken:NSString!
    var items = NSMutableArray()
    var videoTitles = [String]()
    var filteredVideoTitle = [String]()
    var nextVideoId: String!
    final let API_KEY = Constants.youtube.API_KEY
    final let WORD:String = Constants.youtube.WORD
    var resultSearchController = UISearchController()
    var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()

        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height

        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: displayWidth, height: displayHeight - 30 ))

        tableView.autoresizingMask = UIViewAutoresizing.FlexibleTopMargin|UIViewAutoresizing.FlexibleBottomMargin|UIViewAutoresizing.FlexibleWidth
        tableView.delegate = self;
        
        tableView.dataSource = self;
        tableView.opaque = true;
        tableView.backgroundColor = UIColor.clearColor()
        tableView.backgroundView = nil;
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.bounces = false;
        tableView.scrollsToTop = false;
        self.view.addSubview(self.tableView)

        let nibName = UINib(nibName: "PlayListTableViewCell", bundle:nil)
        tableView.registerNib(nibName, forCellReuseIdentifier: "Cell")
        // Do any additional setup after loading the view.
        SVProgressHUD.showWithStatus(Constants.message.LOADING)
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.request(false)
            self.tableView.reloadData()
            SVProgressHUD.dismiss()
        })
        var param : Dictionary<String, String> = ["uuid": (appDelegate.UUID as String)]
        Request.createUser(param)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func request(next: Bool) {
        let searchWord:String! = WORD.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        var urlString:String
        if(next) {
            println(self.nextPageToken)
            urlString = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UCJXNdTefZJ7GzCspNTonM8A&key=\(API_KEY)"
        } else {
            urlString = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UCJXNdTefZJ7GzCspNTonM8A&key=\(API_KEY)"
        }
        let url:NSURL! = NSURL(string:urlString)
        let urlRequest:NSURLRequest = NSURLRequest(URL:url)
        var data = NSURLConnection.sendSynchronousRequest(urlRequest, returningResponse: nil, error: nil)
        let dic:NSDictionary = NSJSONSerialization.JSONObjectWithData(
            data!,
            options: NSJSONReadingOptions.AllowFragments,
            error: nil) as! NSDictionary
        var json = JSON(dic)
        
        var content: Dictionary<String, String> = ["" : ""]
        var prev_videoid:String = ""
        for (index: String, subJson: JSON) in json["items"] {
            var content : Dictionary<String, AnyObject> = ["" : ""]
            
            content[Constants.article_data.VIDEO_ID] = subJson["id"].stringValue
            content[Constants.article_data.TITLE] = subJson["snippet"]["title"].stringValue
            content[Constants.article_data.IMAGE_URL] = subJson["snippet"]["thumbnails"]["default"].stringValue
            content[Constants.article_data.PUBLISH_AT] = subJson["snippet"]["publishedAt"].stringValue
            self.items.addObject(content)
        }

        if(items.count%20 != 0) {
            nextPageToken = nil
        }
        
        self.tableView.reloadData()
    }

    func scrollViewDidScroll(scrollView: UIScrollView) {
        if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)
            && self.nextPageToken != nil
            ) {
                SVProgressHUD.showWithStatus(Constants.message.LOADING)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.request(true)
                    SVProgressHUD.dismiss()
                })
                
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 199
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println(items.count)
        return self.items.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! PlayListTableViewCell
        let item = self.items[indexPath.row] as! NSDictionary

        cell.backgroundColor = UIColor(netHex: 0x3f3f3f)
        cell.titleView.textColor = UIColor.whiteColor()
        cell.titleView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        cell.video_id = item[Constants.article_data.VIDEO_ID] as! String
        
        let imgURL: NSURL? = NSURL(string: item[Constants.article_data.IMAGE_URL] as! String)
        cell.imageView?.setImageWithURL(imgURL)
        var length : String! = "1"
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            var params : Dictionary<String, String> = ["video_id" : item[Constants.article_data.VIDEO_ID] as! String]
            Request.get( Constants.server_api.CREATE_FAVO, params: params) {
                (data, error) in
                length = data["length"].stringValue
                println(length)
                cell.favoriteButton.setTitle(length, forState: .Normal)
            }
        })

        var title = (item[Constants.article_data.TITLE] as? String)!.componentsSeparatedByString("-")
        if(title.count == 1) {
            cell.titleView.text = item[Constants.article_data.TITLE] as? String
        } else {
            cell.titleView.text = title[1]
        }

        cell.sendSubviewToBack(cell.imageView!)
        

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = self.items[indexPath.row] as! NSDictionary
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)

        var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        nextVideoId = item[Constants.article_data.VIDEO_ID] as! String
        println(nextVideoId)
        
//        var param : Dictionary<String, String> = ["video_id" : nextVideoId, "uuid" : (appDelegate.UUID as String)]
//        Request.get("https://edmappback.herokuapp.com/play/create", params: param) {
//            (data, error) in
//            println(data)
//            println("success")
//        }
        
        self.performSegueWithIdentifier("ListVideoViewController", sender: self)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var view : ListVideoViewController = segue.destinationViewController as! ListVideoViewController
        view.channelId = nextVideoId
    }
}
