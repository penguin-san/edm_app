//
//  PlayListTableViewCell.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 6/9/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//

import UIKit

class PlayListTableViewCell: UITableViewCell {

    @IBOutlet weak var playListImage: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    var video_id : String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
