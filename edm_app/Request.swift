//
//  Request.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 5/22/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//
import Foundation


class Request {
    class func get(url_str: String, params: Dictionary<String, String> , callback: (JSON, String?) -> Void){
        var json : JSON!
        var requestbody = ""
        for (key, value) in params {
            var param = key + "=" + value + "&"
            requestbody += param
            
        }
        println(requestbody)
        var url : NSURL = NSURL(string: url_str + "?" + requestbody)!
        println(url)
        var request = NSMutableURLRequest(URL: url)

        request.HTTPMethod = "GET"
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {
            data, responce, error in
            if error != nil {
            }
            json = JSON(data: data)
            callback(json, nil)
        })
        task.resume()
    }
    
    class func createUser(params: Dictionary<String, String>) {
        var json : JSON!
        var requestbody = ""
        for (key, value) in params {
            var param = key + "=" + value + "&"
            requestbody += param
        }
        var url : NSURL = NSURL(string: "http://edmappback.herokuapp.com/user/index?" + requestbody)!
        var request = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "GET"
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: {
            data, responce, error in
            if error != nil {
            }
            json = JSON(data: data)
            println(json)
        })
        task.resume()
    }
}