//
//  VideoViewController.swift
//  edm_app
//
//  Created by 伊藤総一郎 on 5/27/15.
//  Copyright (c) 2015 伊藤総一郎. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    var videoId : String!
    
    @IBOutlet weak var commentView: UITableView!
    @IBOutlet weak var videoPlayer: YTPlayerView!
    var player: KSVideoPlayerView!
    var comment:JSON!
    var loaded = false
    
    @IBAction func postComment(sender: AnyObject) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var start : Float = 0
        var id: String = videoId
        
        RLMRealm()
        
        let nibName = UINib(nibName: "CommentTableViewCell", bundle:nil)
        commentView.registerNib(nibName, forCellReuseIdentifier: "Cell")
        let inputCommentName = UINib(nibName: "InputTableViewCell", bundle:nil)
        commentView.registerNib(inputCommentName, forCellReuseIdentifier: "inputCell")

        commentView.dataSource = self
        commentView.delegate = self
        
        println("video view")
        var srcUrl: NSURL! = NSURL(string: "https://www.youtube.com/watch?v=7Clpf-iSLLo")
        println(srcUrl)
        println("print")
        var dict = HCYoutubeParser.h264videosWithYoutubeURL(srcUrl)
        println(dict["medium"])
        
        self.tabBarController?.tabBar.hidden = true

        var appDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var param : Dictionary<String, String> = ["video_id" : id]
        Request.get(Constants.server_api.GET_COMMENT, params: param) {
            (data, error) in
            println("get comment")
            println(data)
            self.comment = data
            self.loaded = true
            self.commentView.reloadData()
        }
        self.player = KSVideoPlayerView(frame: CGRectMake(0, 20, 320, 300), contentURL: NSURL())
        self.view.addSubview(player)
        player.play()
//        self.videoPlayer.loadWithVideoId(id)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 63
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(loaded == true) {
            println(self.comment.count)
            if(self.comment.count == 0) {
                return 1
            } else {
                return self.comment.count + 1
            }
        } else {
            return 1
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.commentView.dequeueReusableCellWithIdentifier("Cell") as! CommentTableViewCell
        if(indexPath.row == 0) {
            let inputCell = self.commentView.dequeueReusableCellWithIdentifier("inputCell") as! InputTableViewCell
            inputCell.video_id = videoId
            return inputCell
        } else {
            if(loaded == true) {
                let item = self.comment[indexPath.row]
                cell.contentLabel.text = item["content"].stringValue
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    
}